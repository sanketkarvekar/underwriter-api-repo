package com.inscomp.UnderwriterAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnderwriterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnderwriterApiApplication.class, args);
	}

}
